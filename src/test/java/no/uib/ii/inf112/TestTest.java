package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import no.uib.ii.inf112.TextAligner;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length());
			if (extra % 2 == 0) {
				extra = extra / 2;
				return " ".repeat(extra) + text + " ".repeat(extra);
			} else {
				extra = extra / 2;
				return " ".repeat(extra) + text + " ".repeat(extra) + " ";
			}
			
			
		}

		public String flushRight(String text, int width) {
			int extra = (width - text.length());
			return text + " ".repeat(extra);
		}

		public String flushLeft(String text, int width) {
			int extra = (width - text.length());
			return " ".repeat(extra) + text;
		}

		public String justify(String text, int width) {
			String[] words = text.split(" ");
			if (words.length == 1) {
				return center(text, width);
			} else if (words.length == 2) {
				String extra;
				if (width % 2 == 0) {
					extra = "";
				} else {
					extra = " ";
				}
				return flushRight(words[0], width / 2) + extra + flushLeft(words[1], width / 2);	
			} else if (words.length == 3) {
				String extra;
				if (width % 2 == 0) {
					extra = "";
				} else {
					extra = " ";
				}
				return flushRight(words[0], width / 3) + extra + words[1] + extra + flushLeft(words[2], width / 3);
			} else {
				return null;
			}
		}};


	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals("foo ", aligner.center("foo", 4));
		assertEquals(" foo  ", aligner.center("foo", 6));
	}
	
	@Test
	void testFlushRight() {
		assertEquals("A    ", aligner.flushRight("A", 5));
		assertEquals("foo ", aligner.flushRight("foo", 4));
	}
	
	@Test
	void testFlushLeft() {
		assertEquals("    A", aligner.flushLeft("A", 5));
		assertEquals(" foo", aligner.flushLeft("foo", 4));
	}
	
	@Test
	void testJustify() {
		assertEquals("      fie      ", aligner.justify("fie", 15));
		assertEquals("fee         foo", aligner.justify("fee foo", 15));
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
	}
}
